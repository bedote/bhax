package com.example.myapplication;

public class Animal extends Creature implements Eat{

    protected boolean run;

    public Animal(String name, boolean run) {
        super(name);
        this.run = run;
    }



    @Override
    public boolean eat() {
        return false;
    }

    @Override
    boolean getLife() {
        return false;
    }
}

