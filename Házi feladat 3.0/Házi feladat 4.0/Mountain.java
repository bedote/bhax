package com.example.game;

public class Mountain extends TerrainItem {

protected  int crowd;

    public Mountain( boolean use,int crowd) {
        super( use);
        this.crowd=crowd;
    }

    public int getCrowd() {
        return crowd;
    }

    public void setCrowd(int crowd) {
        this.crowd = crowd;
    }
}
