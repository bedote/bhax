package com.example.myapplication;

 abstract public class Creature {

    protected boolean live;

    protected  String name;


     public Creature(String name) {
         this.name = name;
     }

    public boolean life() {
        live = getLife();
        return true ;
    }

     abstract boolean getLife();

}
