package com.example.myapplication;

public class User extends Creature implements Eat{

    protected boolean speak;

    public User(String name, boolean speak) {
        super(name,5);
        this.speak = speak;
    }

    @Override
    public boolean eat() {
        return true;
    }

    public void working() {
        this.setHealth(this.getHealth()-1);
    }

    public void eating() {
        this.setHealth(this.getHealth()+1);
    }
}
