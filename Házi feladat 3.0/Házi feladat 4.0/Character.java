package com.example.game;

public class Character extends Game {

    protected double range;

    public Character( double range) {
        super(1);
        this.range = range;
    }

    public double getRange() {
        return range;
    }

    public void setRange(double range) {
        this.range = range;
    }

    public void eating(int health) {
        this.setHealth(getHealth() + 1);
    }

    public void dieing(int health) {
        this.setHealth(getHealth() - 1);
    }

    @Override
    public String toString() {
        return "Character:" +  "range=" + range + ", health=" + health ;
    }
}