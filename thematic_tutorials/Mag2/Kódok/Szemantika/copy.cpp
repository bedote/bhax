#include <cstdio>

class Array
{
public:
    Array(size_t elementCount):
        elementCount(elementCount), elements(new int[elementCount])
    {}

    Array(const Array &other):
        elementCount(other.elementCount), elements(new int[other.elementCount])
    {
        std::printf("Copy Constructor\n");

        for (size_t i = 0; i < this->elementCount; ++i)
        {
            this->elements[i] = other.elements[i];
        }
    }

    ~Array()
    {
        std::printf("Array Destructor\n");
        std::printf("%X\n", elements);
        delete []elements;
    }

    Array &operator=(const Array &other)
    {
        std::printf("Copy Assignment\n");

        if (&other != this)
        {
            this->elementCount = other.elementCount;

            delete []this->elements;

            this->elements = new int[this->elementCount];

            for (size_t i = 0; i < this->elementCount; ++i)
            {
                this->elements[i] = other.elements[i];
            }
        }

        return *this;
    }

    void fillWith(int value)
    {
        for (size_t i = 0; i < elementCount; ++i)
        {
            elements[i] = value;
        }
    }

    void print()
    {
        for (size_t i = 0; i < elementCount; ++i)
        {
            std::printf("%d\n", elements[i]);
        }
    }

    size_t elementCount;
    int *elements;
};

int main(int argc, char **argv)
{
    Array first(5);
    first.fillWith(5);

    Array second(first);

    first.print();
    second.print();

    return 0;
}
