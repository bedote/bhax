package com.example.game;

public class TerrainItem  extends Game{

    protected boolean use;

    public TerrainItem(boolean use) {
        super(2);
        this.use=use;
    }

    public boolean isUse() {
        return use;
    }

    public void setUse(boolean use) {
        this.use = use;
    }

    public void damaging(int health) {
        this.setHealth(getHealth() -1);
    }

    public void developing(int health) {
        this.setHealth(getHealth() + 1);
    }

    @Override
    public String toString() {
        return "TerrainItem:" + "use=" + use + ", health=" + health;
    }
}
