package com.example.myapplication;

public class Animal extends Creature implements Eat{

    protected boolean run;

    public Animal(String name, boolean run) {
        super(name,20);
        this.run = run;

    }
    
    @Override
    public boolean eat() {
        return false;
    }

    public void eating() {
        this.setHealth(this.getHealth()+1);
    }

    public void hunting() {
        this.setHealth(this.getHealth()-1);
    }
}

