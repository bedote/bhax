package com.example.myapplication;

public class Plant extends Creature implements Photosynthesis {

    public Plant(String name) {
        super(name,10);
    }

    @Override
    public boolean photosynthesis() {
        return false;
    }

    public void growing(){
        System.out.println("growing");
        this.setHealth(this.getHealth()-1);
    }

    public void photosynthesizing(){
        System.out.println("photosynthesizing");
        this.setHealth(this.getHealth()+1);
    }
}

