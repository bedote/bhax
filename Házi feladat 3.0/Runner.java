package com.example.myapplication;

import java.util.*;
import java.util.List;

public class Runner {
    
    public static  void main(String[] args){

        Animal pig =  new Animal("Marci",true);
        Animal deer = new Animal("Deni",true);
        User man = new User("Donát",true);
        User woman = new User("Zsófi",true);
        Plant flower = new Plant("Narcissus");
        Plant tree = new Plant("Acacias");

        Creature[] creatureList = { pig, deer, man, woman, flower,tree};
            List<Creature> creatureResult = Arrays.asList(creatureList);
            Collections.sort(creatureResult);
            System.out.println(creatureResult);
            
    }

}
