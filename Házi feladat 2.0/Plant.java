package com.example.myapplication;

public class Plant extends Creature implements Photosynthesis {

    public Plant(String name) {
        super(name);
    }

    @Override
    boolean getLive() {
        return false;
    }

    @Override
    public boolean photosynthesis() {
        return false;
    }
}

