package com.example.game;


public class Tower extends Building {

    protected boolean shot;

    public Tower( boolean move,boolean shot) {
        super(move);
        this.shot=shot;
    }

    public boolean isShot() {
        return shot;
    }

    public void setShot(boolean shot) {
        this.shot = shot;
    }

}
