package com.example.game;

public class Wall extends Building  {

    protected String protection;

    public Wall( boolean move,String protection) {
        super(move);
        this.protection=protection;
    }

    public String getProtection() {
        return protection;
    }

    public void setProtection(String protection) {
        this.protection = protection;
    }


}
