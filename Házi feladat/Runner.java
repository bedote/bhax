package com.example.myapplication;

import java.util.*;
import java.util.ArrayList;
import java.util.List;

public class Runner {
    public static void main(){
        Animal pig =  new Animal("Apple","Yes");
        Animal deer = new Animal("Grass","Yes");
        User man = new User("Steak","Yes");
        User woman = new User("pasta","No");
        Plant flower = new Plant("Yes","Narcissus");
        Plant tree = new Plant("Yes","Acacias");


        List<Creature> creatureList= new ArrayList<>();

        creatureList.add(pig);
        creatureList.add(deer);
        creatureList.add(man);
        creatureList.add(woman);
        creatureList.add(tree);
        creatureList.add(flower);
        
        Collections.sort(creatureList);
        
    }

}
