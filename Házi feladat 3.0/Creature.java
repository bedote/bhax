package com.example.myapplication;

 abstract public class Creature implements Comparable<Creature> {

    protected  String name;

    protected int health;

     public Creature(String name, int health) {
         this.name = name;
         this.health = health;
     }

     public int getHealth(){
         return this.health;
     }

     public void setHealth(int newHealth){
         this.health= newHealth;
     }

     @Override
     public int compareTo(Creature value){
         return this.name.compareTo(value.name);
     }

     @Override
     public String toString(){
         return this.name;
     }

 }





