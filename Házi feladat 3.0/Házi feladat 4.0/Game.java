package com.example.game;

public abstract class Game{

    protected  int health;

    public Game(int health){
        this.health =health;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
}
