package com.example.game;

public class Tree extends TerrainItem {

    protected String name;

    public Tree(boolean use,String name) {
        super(use);
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
