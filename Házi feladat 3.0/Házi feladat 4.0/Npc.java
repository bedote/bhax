package com.example.game;

public class Npc extends Character{

   protected boolean prohibited;

    public Npc(double range,boolean prohibited) {
        super( range);
        this.prohibited=prohibited;
    }

    public boolean isProhibited() {
        return prohibited;
    }

    public void setProhibited(boolean prohibited) {
        this.prohibited = prohibited;
    }
}
