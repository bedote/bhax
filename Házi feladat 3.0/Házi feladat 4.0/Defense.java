package com.example.game;

public class Defense  extends  Building{

    protected String type;

    public Defense( boolean move,String type) {
        super( move);
        this.type=type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
