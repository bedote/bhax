package com.example.myapplication;

public class User extends Creature implements Eat{

    protected boolean speak;

    public User(String name, boolean speak) {
        super(name);
        this.speak = speak;
    }

    @Override
    public boolean eat() {
        return true;
    }

    @Override
    boolean getLife() {
        return false;
    }
}
