#include <cstdio>

class Array
{
public:
    Array(size_t elementCount):
        elementCount(elementCount), elements(new int[elementCount])
    {
        std::printf("Array Constructor\n");
    }

    Array(const Array &other):
        elementCount(other.elementCount), elements(new int[other.elementCount])
    {
        std::printf("Copy Constructor\n");

        for (size_t i = 0; i < this->elementCount; ++i)
        {
            this->elements[i] = other.elements[i];
        }
    }

    ~Array()
    {
        std::printf("Array Destructor\n");
        std::printf("%X\n", elements);
        delete []elements;
    }

    Array &operator=(const Array &other)
    {
        std::printf("Copy Assignment\n");

        if (&other != this)
        {
            this->elementCount = other.elementCount;

            delete []this->elements;

            this->elements = new int[this->elementCount];

            for (size_t i = 0; i < this->elementCount; ++i)
            {
                this->elements[i] = other.elements[i];
            }
        }

        return *this;
    }

    Array &operator=(Array &&other)
    {
        std::printf("Array Move Assignment\n");

        if (&other != this)
        {
            this->elementCount = other.elementCount;
            this->elements = other.elements;

            other.elements = nullptr;
        }

        return *this;
    }

    void fillWith(int value)
    {
        for (size_t i = 0; i < elementCount; ++i)
        {
            elements[i] = value;
        }
    }

    void print()
    {
        for (size_t i = 0; i < elementCount; ++i)
        {
            std::printf("%d\n", elements[i]);
        }
    }

    size_t elementCount;
    int *elements;
};

Array multiply(Array &array, int multiplier);

int main(int argc, char **argv)
{
    Array first(5);

    first.fillWith(5);
    first.print();
    first = multiply(first, 5);

    std::printf("First %X\n", first.elements);

    first.print();

    return 0;
}

Array multiply(Array &array, int multiplier)
{
    Array multiplied(array.elementCount);

    for (size_t i = 0; i < multiplied.elementCount; ++i)
    {
        multiplied.elements[i] = array.elements[i] * multiplier;
    }

    std::printf("Multiplied %X\n", multiplied.elements);

    return multiplied;
}
