package com.example.game;

public class Building extends Game {

    protected boolean move;

    public Building(boolean move) {
        super(10 );
        this.move=move;
    }

    public boolean isMove() {
        return move;
    }

    public void setMove(boolean move) {
        this.move = move;
    }

    public void developing (int health) {
        this.setHealth(getHealth()+1);
    }

    public void shoting (int health){
        this.setHealth(getHealth()-1);
    }

    @Override
    public String toString() {
        return "Building:" + "move=" + move + ", health=" + health ;
    }
}

