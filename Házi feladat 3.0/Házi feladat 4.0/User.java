package com.example.game;

public class User extends Character{

    protected int points;

    public User(double range,int ponints) {
        super(range);
        this.points=ponints;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
