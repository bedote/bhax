package com.example.game;

public class Runner {

    public static  void  main(String [] args){

        Character character= new Character(10);
        System.out.println(character.toString());

        Building building = new Building(true);
        System.out.println(building.toString());

        TerrainItem terrainItem = new TerrainItem(false);
        System.out.println(terrainItem.toString());

    }

}
