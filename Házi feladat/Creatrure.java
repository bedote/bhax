package com.example.myapplication;

 abstract public class Creature {

    protected String live;

    public boolean  eat () {
        live = getLive();
        return true;
    }

     abstract  String getLive();

}